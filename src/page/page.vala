public class Slate.Page.Page : Object, Slate.Interface.Page.Page {
	
	protected Slate.Interface.Settings.Provider? persistant_settings = null;
	protected Slate.Settings.RamProvider local_settings = new Slate.Settings.RamProvider();
	protected Slate.Settings.Context.Fallback settings_context = new Slate.Settings.Context.Fallback();
	protected Slate.Interface.Page.Service.Metadata metadata;
	protected Slate.Interface.Page.Service.Syncronisation syncronisation;
	protected Slate.Interface.Page.Service.InternalNavigation internal_navigation;
	protected Slate.Interface.Page.Service.ExternalNavigation external_navigation;
	protected Slate.Interface.Page.Service.Tasks.Service task_service = new Slate.Page.Service.Task();
	protected Slate.Interface.Page.Service.PagePlugin.Index page_plugin_index;
	protected Slate.Interface.Page.Service.LinearHistory? linear_history = null;
	protected Slate.Interface.Page.Service.Resource.Service resource_service;
	protected Slate.Interface.Page.Service.Resource.Loader resource_loader;
	protected Slate.Interface.Page.Service.Transaction.Log transaction_log;
	protected Slate.Interface.Page.Service.Document.Store document_store;
	protected Slate.Interface.Page.Service.Containers? containers_service = null;
	protected string? own_container_id = null;
	protected Slate.Interface.Page.Service.Dialog? dialog_ui_service = null;
	
	
	
	public Page(Slate.Interface.Page.Service.Resource.Loader resource_loader, Slate.Interface.Page.Service.ExternalNavigation external_navigation, Slate.Interface.Settings.Provider? persistant_settings = null, Slate.Interface.Page.Service.LinearHistory? linear_history = null, Slate.Interface.Page.Service.Syncronisation? syncronisation = null, string? bootstrap_uri = null, Slate.Interface.Page.Service.Containers? containers_service = null, string? own_container_id = null, Slate.Interface.Page.Service.PagePlugin.Index? page_plugin_index = null, string[] default_page_plugins = {}){
		this.resource_loader = resource_loader;
		this.external_navigation = external_navigation;
		this.persistant_settings = persistant_settings;
		this.linear_history = linear_history;
		this.containers_service = containers_service;
		this.own_container_id = own_container_id;
		this.resource_service = new Slate.Page.Service.Resource();
		this.transaction_log = new Slate.Page.Service.Transaction.Log(this.task_service);
		this.document_store = new Slate.Page.Service.Document.Store();
		//assigning to a seperate var because the object specific load page call is used below
		var internal_navigation = new Slate.Page.Service.InternalNavigation(this.resource_loader, this.resource_service, this.transaction_log);
		this.internal_navigation = internal_navigation;
		this.metadata = new Slate.Page.Service.Metadata();
		if (syncronisation != null){
			this.syncronisation = syncronisation;
		} else {
			this.syncronisation = new Slate.Page.Service.Syncronisation();
		}
		this.settings_context.add_fallback(local_settings);
		if (persistant_settings != null) {
			this.settings_context.add_fallback(persistant_settings);
		}
		dialog_ui_service = new Slate.Page.Service.Dialog();
		if (page_plugin_index == null) {
			this.page_plugin_index = new Slate.Page.Service.PagePluginIndex();
		} else {
			this.page_plugin_index = page_plugin_index;
		}
		//load default page plugins
		foreach (string plugin_id in default_page_plugins) {
			this.try_load_plugin(plugin_id);
		}
		// load first page
		if (bootstrap_uri != null) {
			internal_navigation.load_page(bootstrap_uri);
		}
	}
	
	public bool try_load_plugin(string plugin_id){
		var loader = this.page_plugin_index.get_plugin_loader(plugin_id);
		if (loader != null) {
			return loader.load_plugin(this);
		} else {
			return false;
		}
	}
	
	  ///////////////////////////////
	 // Slate.Interface.Page.Page //
	///////////////////////////////
	
	public virtual Slate.Interface.Settings.Provider get_page_settings_provider(){
		return settings_context;
	}
	
	public virtual Slate.Interface.Settings.Provider? get_persistant_page_settings_provider(){
		return persistant_settings;
	}
	
	//Core services
	public virtual Slate.Interface.Page.Service.Metadata get_metadata_service(){
		return metadata;
	}
	
	public virtual Slate.Interface.Page.Service.Syncronisation get_syncronisation_service(){
		return syncronisation;
	}
	
	public virtual Slate.Interface.Page.Service.InternalNavigation get_internal_navigation_service(){
		return internal_navigation;
	}
	
	public virtual Slate.Interface.Page.Service.ExternalNavigation get_external_navigation_service(){
		return external_navigation;
	}
	
	public virtual Slate.Interface.Page.Service.Tasks.Service get_tasks_service(){
		return task_service;
	}
	
	public virtual Slate.Interface.Page.Service.PagePlugin.Index get_page_plugin_index_service(){
		return page_plugin_index;
	}
	
	//optinal services
	public virtual Slate.Interface.Page.Service.LinearHistory? get_linear_history_service(){
		return linear_history;
	}
	
	public virtual Slate.Interface.Page.Service.Resource.Service? get_resource_service(){
		return resource_service;
	}
	
	public virtual Slate.Interface.Page.Service.Resource.Loader? get_resource_loader_service(){
		return resource_loader;
	}
	
	public virtual Slate.Interface.Page.Service.Transaction.Log? get_transaction_log_service(){
		return transaction_log;
	}
	
	public virtual Slate.Interface.Page.Service.Document.Store? get_document_store_service(){
		return document_store;
	}
	
	public virtual Slate.Interface.Page.Service.Containers? get_continers_service(){
		return containers_service;
	}
	
	public virtual Slate.Interface.Page.Container? get_own_container(){
		if (containers_service != null && own_container_id != null) {
			return containers_service.get_contatiner(own_container_id);
		} else {
			return null;
		}
	}
	
	public virtual string? get_own_container_id(){
		return own_container_id;
	}
	
	//optional ui services
	public virtual Slate.Interface.Page.Service.Dialog? get_dialog_ui_service(){
		return dialog_ui_service;
	}
}
