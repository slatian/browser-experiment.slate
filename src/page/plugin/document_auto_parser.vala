public class Slate.Page.Plugin.DocumentAutoParser : Slate.Interface.Page.Service.Tasks.Task, Object {
	
	public const string PAGE_PLUGIN_ID = "slate.document_auto_parser";
	public const string VERSION = "builtin";
	
	private Slate.Interface.Page.Service.Document.Store document_store_service;
	private Slate.Interface.Page.Service.Tasks.Service task_service;
	private Slate.Interface.Page.Service.Resource.Service resource_service;
	private string state = "initalizing";
	private string? task_id = null;
	
	public DocumentAutoParser(Slate.Interface.Page.Service.Tasks.Service task_service, Slate.Interface.Page.Service.Resource.Service resource_service, Slate.Interface.Page.Service.Document.Store document_store_service){
		this.resource_service = resource_service;
		this.document_store_service = document_store_service;
		this.task_service = task_service;
		// hook signals
		resource_service.resource_updated.connect(on_resource_updated);
		state = "idle";
		task_id = task_service.add_task_object(this);
	}
	
	~DocumentAutoParser(){
		resource_service.resource_updated.disconnect(on_resource_updated);
	}
	
	private void on_resource_updated(string resource_name, Slate.Interface.Page.Service.Resource.Resource? resource){
		document_store_service.remove_document("slate.document_auto_parser<"+resource_name);
		if (resource != null){
			if (resource.is_multiuse()) {
				string? mimetype = resource.get_metadata("mimetype");
				if (mimetype != null) {
					Slate.Document.SlateDocument.Interface.Parser? parser = null;
					// TODO: replace this with a proper parser selection mechanism
					debug(@"Got mimetype: $mimetype");
					if (mimetype.has_prefix("text/gemini")) {
						parser = new Slate.Document.SlateDocument.Parser.Gemini(resource);
					} else if(mimetype.has_prefix("text/dragonstone-directory")){
						parser = new Slate.Document.SlateDocument.Parser.DragonstoneFileListing(resource);
					} else if(mimetype.has_prefix("application/gopher") || mimetype.has_prefix("text/gopher")){
						parser = new Slate.Document.SlateDocument.Parser.GopherMap(resource, new Slate.Settings.Model.Gopher.TypeRegistry.default_configuration());
					}
					if (parser != null) {
						parser.on_root_node_updated.connect((node) => {
							document_store_service.store_document("slate.document_auto_parser<"+resource_name, node);
						});
						parser.on_finished.connect((node) => {
							document_store_service.store_document("slate.document_auto_parser<"+resource_name, node);
						});
						parser.process.begin(null);
						return;
					}
				}
			}
		}
	}
	
	  /////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Tasks.Task //
	/////////////////////////////////////////////
	
	public string get_task_name(){
		return PAGE_PLUGIN_ID;
	}
	
	public string get_task_category(){
		return "ui";
	}
	
	public double? get_task_progress(){
		return null;
	}
	
	public string get_task_state(){
		return this.state;
	}
	
	public bool has_task_finished(){
		return state == "initalizing";
	}
	
	public string[] get_available_task_actions(){
		return {};
	}
	
	public bool activate_task_action(string action){
		return false;
	}
	
	public void foreach_task_property(Func<string> cb){
		// no task properties
	}
	
	public string? read_task_property(string key){
		switch(key) {
			default:
				return null;
		}
	}
}
