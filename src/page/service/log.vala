public class Slate.Page.Service.Log : Object, Slate.Interface.Page.Service.Log.Service {
	
	private static uint hash(uint64? key){
		if (key == null) {
			return 0;
		} else {
			return (uint) key%1073741824;
		}
	}
	
	private static bool equal(uint64? a, uint64? b){
		return a == b;
	}
	
	//Expecting a lot of entrys so a hashmap may be cheaper in the long run
	private HashTable<uint64?,Slate.Interface.Page.Service.Log.Entry> entrys = new HashTable<uint64?,Slate.Interface.Page.Service.Log.Entry>(hash, equal);
	private uint64 counter = 0;
	
	  ////////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.Log.Service //
	////////////////////////////////////////////////////
	
	public void submit_entry(Slate.Interface.Page.Service.Log.Entry entry){
		uint64 num;
		lock(counter) {
			entrys.set(counter, entry);
			num = counter;
			counter++;
		}
		this.entry_submitted(entry, num);
	}
	
	public uint64 get_number_of_submitted_entrys(){
		return counter;
	}
	
	public Slate.Interface.Page.Service.Log.Entry? get_nth_entry(uint64 num){
		return entrys.get(num);
	}
}
