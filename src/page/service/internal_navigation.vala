public class Slate.Page.Service.InternalNavigation : Object, Slate.Interface.Page.Service.InternalNavigation {
	
	private Slate.Interface.Page.Service.Resource.Loader loader;
	private Slate.Interface.Page.Service.Resource.Service resource_service;
	private Slate.Interface.Page.Service.Transaction.Log transaction_log;
	private string current_uri = "";
	private uint redirect_counter = 0;
	
	public InternalNavigation(Slate.Interface.Page.Service.Resource.Loader loader, Slate.Interface.Page.Service.Resource.Service resource_service, Slate.Interface.Page.Service.Transaction.Log transaction_log){
		this.loader = loader;
		this.resource_service = resource_service;
		this.transaction_log = transaction_log;
		transaction_log.resource_added.connect(on_resource_added);
	}
	
	~InternalNavigation(){
		transaction_log.resource_added.disconnect(on_resource_added);
	}
	
	// This returns a bool, so that the compiler will complain when the loader api changes
	public bool load_page(string uri, bool reload = false){
		if (this.current_uri != uri) {
			this.current_uri = uri;
			current_uri_changed(uri);
		}
		return this.loader.load_resource(uri, reload, transaction_log) != null;
	}
	
	//Patch the main resource trough to "root_page_source" resource on the resource panel
	
	private void on_resource_added(string resource_id, Slate.Interface.Page.Service.Transaction.Resource resource){
		if (resource.uri == current_uri){
			this.resource_service.set_resource("root_page_source", resource.resource);
		}
	}
	
	  /////////////////////////////////////////////////////
	 // Slate.Interface.Page.Service.InternalNavigation //
	/////////////////////////////////////////////////////
	
	// Where we are now
	public string get_current_uri(){
		return current_uri;
	}
	
	// redirecting
	public void redirect(string uri, string module_name){
		redirect_counter++;
		load_page(uri);
	}
	
	public uint get_redirect_counter(){
		return redirect_counter;
	}
	
	public bool may_autoredirect_to(string uri){
		if (redirect_counter > 4) {
			return false;
		}
		if (uri == current_uri+"/" || uri+"/" == current_uri){
			return true;
		}
		return false;
	}
	
	// reloading
	public void reload(){
		load_page(current_uri, true);
	}
	
}
