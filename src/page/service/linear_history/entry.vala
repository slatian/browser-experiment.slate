public class Slate.Page.Service.LinearHistory.Entry : Object {
	public string uri;
	//preserve the syncronisatio service in the history and reuse it when recreating the page
	public Slate.Interface.Page.Service.Syncronisation syncronisation;
	
	public Entry(string uri, Slate.Interface.Page.Service.Syncronisation syncronisation){
		this.uri = uri;
		this.syncronisation = syncronisation;
	}
}
