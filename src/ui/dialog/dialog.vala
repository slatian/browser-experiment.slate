public class Slate.Ui.Dialog.Dialog : Slate.Interface.Ui.Dialog, Object {
	
	protected string name = "dummy";
	protected string? message = null;
	protected bool has_primary = false;
	protected Slate.Interface.Ui.DialogAction[] actions = {};
	
	public Dialog(string name, string? message, bool has_primary_action, Slate.Interface.Ui.DialogAction[] actions){
		this.name = name;
		this.message = message;
		this.has_primary = has_primary_action;
		this.actions = actions;
	}
	
	  ///////////////////////////////
	 // Slate.Interface.Ui.Dialog //
	///////////////////////////////
	
	public string get_dialog_name(){
		return name;
	}
	
	public string? get_dialog_message(){
		return message;
	}
	
	public bool has_primary_action(){
		return has_primary;
	}
	
	public Slate.Interface.Ui.DialogAction[] get_actions(){
		return actions;
	}
}
