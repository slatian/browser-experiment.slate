public class Slate.Util.UriPattern : Object {
	
	// A name that can be used to store arbitrary data in the uri rule, that somehow identifies the rule
	// This exists to avoid needing a ton of wrapper classes that just add a string
	public string? rule_name = null;
	
	// higher score means higher significance
	// must be set from an external source as its meaning is context dependent
	// (ranked rule list, derived from specifitys)
	public uint32 rule_score = 0;
	
	public string? scheme_match = null;
	public bool scheme_exact_match = false;
	public bool scheme_prefix_match = false;
	public bool scheme_suffix_match = false;
	
	public string? username_match = null;
	public bool username_exact_match = false;
	public bool username_specified_match = false;
	
	public string? hostname_match = null;
	public bool hostname_exact_match = false;
	public bool hostname_prefix_match = false;
	public bool hostname_suffix_match = false;
	
	public string? port_match = null;
	public bool port_invert_match = false;
	public bool port_unspecified_match = false;
	
	public string[]? path_pattern = null;
	
	public string? query_pattern = null;
	public string? index_pattern = null;
	
	public UriPattern.from_string(string uri_pattern, uint32 score = 0, string? name = null){
		rule_from_uri_pattern(new Slate.Util.Uri(uri_pattern));
		this.rule_score = score;
		this.rule_name = name;
	}
	
	public UriPattern.from_uri(Slate.Util.Uri uri_pattern, uint32 score = 0, string? name = null){
		rule_from_uri_pattern(uri_pattern);
		this.rule_score = score;
		this.rule_name = name;
	}
	
	public bool matches_scheme(string? scheme){
		if (scheme_exact_match) {
			if (scheme == scheme_match) {
				return true;
			}
		}
		if (scheme_prefix_match) {
			if (scheme.has_prefix(scheme_match+"+")) {
				return true;
			}
		}
		if (scheme_suffix_match) {
			if (scheme.has_suffix("+"+scheme_match)) {
				return true;
			}
		}
		// return true if no rule was specified
		return !(scheme_exact_match || scheme_prefix_match || scheme_suffix_match);
	}
	
	public bool matches_username(string? username){
		if (username_specified_match) {
			if (username != null) {
				return true;
			}
		}
		if (username_exact_match) {
			if (username == username_match) {
				return true;
			}
		}
		return !(username_exact_match || username_specified_match);
	}
	
	public bool matches_hostname(string? hostname){
		if (hostname_exact_match) {
			if (hostname == hostname_match){
				return true;
			}
		}
		if (hostname_prefix_match && hostname != null) {
			if (hostname.has_prefix(hostname_match+".")) {
				return true;
			}
		}
		if (hostname_suffix_match && hostname != null) {
			if (hostname.has_suffix("."+hostname_match)) {
				return true;
			}
		}
		// return true if no rule was specified
		return !(hostname_exact_match || hostname_prefix_match || hostname_suffix_match);
	}
	
	public bool matches_port(string? port){
		if (port_unspecified_match) {
			if (port == null) {
				return true;
			}
		}
		if (port == port_match) {
			return !port_invert_match;
		} else {
			if (port != null) {
				return port_invert_match;
			}
		}
		return false;
	}
	
	public bool matches_path(string? path){
		if (path_pattern == null) {
			return true;
		} else if (path == null) {
			return false;
		} else if (path_pattern.length == 0) {
			return path == "";
		} else {
			uint pattern_index = 0;
			string[] segments = path.split("/");
			uint segment_count = 0;
			bool wildcard = false;
			foreach(string segment in segments) {
				//debug("Matching path segment: "+segment);
				segment_count++;
				string current_pattern = path_pattern[pattern_index];
				while (current_pattern == "**") {
					pattern_index++;
					wildcard = true;
					//debug("WILDCARD!");
					if (pattern_index >= path_pattern.length) { return true; }
					current_pattern = path_pattern[pattern_index];
				}
				//debug("Against pattern: "+current_pattern);
				if (match_prefix_suffix_pattern(current_pattern,segment)) {
					wildcard = false;
					pattern_index++;
				} else if (wildcard) {
					//debug("Wildcard match");
				} else {
					//debug("Mismatch");
					return false;
				}
				if (pattern_index >= path_pattern.length) {
					return segment_count == segments.length;
				}
			}
			return pattern_index == path_pattern.length;
		}
	}
	
	public bool matches_query(string? query){
		if (query_pattern == null) {
			return true;
		} else if (query_pattern == "*") {
			return query != null;
		} else if (query_pattern == "!*") {
			return query == null;
		} else {
			return match_prefix_suffix_pattern(query_pattern, query.replace("!","%21"));
		}
	}
	
	public bool matches_index(string? index){
		if (index_pattern == null) {
			return true;
		} else if (index_pattern == "*") {
			return index != null;
		} else if (index_pattern == "!*") {
			return index == null;
		} else {
			return match_prefix_suffix_pattern(index_pattern, index.replace("!","%21"));
		}
	}
	
	public bool match_uri(Slate.Util.Uri uri){
		//debug("Matching " + uri.uri);
		if (!matches_scheme(uri.scheme)) { /*debug("Failed on scheme");/**/ return false; }
		if (!matches_username(uri.username)) { /*debug("Failed on username");/**/ return false; }
		if (!matches_hostname(uri.hostname)) { /*debug("Failed on hostname");/**/ return false; }
		if (!matches_port(uri.port)) { /*debug("Failed on port");/**/ return false; }
		if (!matches_path(uri.path)) { /*debug("Failed on path");/**/ return false; }
		if (!matches_query(uri.query)) { /*debug("Failed on query");/**/ return false; }
		if (!matches_index(uri.index)) { /*debug("Failed on index");/**/ return false; }
		//debug("Passed!");
		return true;
	}
	
	public void scheme_match_from_string(string? pattern){
		if (pattern == null) {
			scheme_exact_match = false;
			scheme_prefix_match = false;
			scheme_suffix_match = false;
			scheme_match = null;
		} else if (pattern.has_prefix("+") && pattern.length > 1) {
			scheme_prefix_match = false;
			scheme_exact_match = false;
			scheme_suffix_match = true;
			scheme_match = pattern.substring(1);
		} else if (pattern.has_suffix("+") && pattern.length > 1) {
			scheme_prefix_match = true;
			scheme_exact_match = true;
			scheme_suffix_match = false;
			scheme_match = pattern.substring(0,pattern.length-1);
		} else {
			scheme_prefix_match = false;
			scheme_exact_match = true;
			scheme_suffix_match = false;
			scheme_match = pattern;
		}
	}
	
	public void username_match_from_string(string? pattern){
		if (pattern == null) {
			username_specified_match = true;
			username_exact_match = true;
			username_match = null;
		} else if (pattern == "*") {
			username_specified_match = true;
			username_exact_match = false;
			username_match = null;
		} else if (pattern == "!*") {
			username_specified_match = false;
			username_exact_match = true;
			username_match = null;
		} else {
			username_specified_match = false;
			username_exact_match = true;
			username_match = pattern;
		}
	}
	
	public void hostname_match_from_string(string? pattern){
		if (pattern == null) {
			hostname_exact_match = false;
			hostname_prefix_match = false;
			hostname_suffix_match = false;
			hostname_match = null;
		} else if (pattern == "*") {
			hostname_exact_match = false;
			hostname_prefix_match = true;
			hostname_suffix_match = false;
			hostname_match = "";
		} else if (pattern == "!*") {
			hostname_exact_match = true;
			hostname_prefix_match = false;
			hostname_suffix_match = false;
			hostname_match = null;
		} else if (pattern.has_prefix(".") && pattern.length > 1) {
			hostname_exact_match = false;
			hostname_prefix_match = false;
			hostname_suffix_match = true;
			hostname_match = pattern.substring(1);
		} else if (pattern.has_suffix(".") && pattern.length > 1) {
			hostname_exact_match = true;
			hostname_prefix_match = true;
			hostname_suffix_match = false;
			hostname_match = pattern.substring(0,pattern.length-1);
		} else {
			hostname_exact_match = true;
			hostname_prefix_match = false;
			hostname_suffix_match = false;
			hostname_match = pattern;
		}
	}
	
	public void port_match_from_string(string? pattern){
		if (pattern == null){
			port_invert_match = true;
			port_unspecified_match = true;
			port_match = null;
		} else if (pattern == "*" || pattern.has_prefix("*!")) {
			port_invert_match = true;
			port_unspecified_match = false;
			port_match = null;
			if (pattern.length >2) {
				port_match = pattern.substring(2);
			}
		} else if (pattern.has_prefix("!*")) {
			port_invert_match = false;
			port_unspecified_match = true;
			port_match = null;
			if (pattern.length >2) {
				port_match = pattern.substring(2);
			}
		} else if (pattern.has_prefix("!") && pattern.length > 1) {
			port_invert_match = true;
			port_unspecified_match = true;
			port_match = pattern.substring(1);
		} else {
			port_invert_match = false;
			port_unspecified_match = false;
			port_match = pattern;
		}
	}
	
	public void path_match_from_string(string? pattern){
		if (pattern == null || pattern == "") {
			path_pattern = null;
		} else {
			path_pattern = pattern.split("/");
		}
	}
	
	public void rule_from_uri_pattern(Slate.Util.Uri uri){
		scheme_match_from_string(uri.scheme);
		username_match_from_string(uri.username);
		hostname_match_from_string(uri.hostname);
		port_match_from_string(uri.port);
		path_match_from_string(uri.path);
		query_pattern = uri.query;
		index_pattern = uri.index;
	}
	
	public static bool match_prefix_suffix_pattern(string pattern, string content) {
		if (pattern == "") {
			return content == "";
		}
		var splittage = pattern.split("*",2);
		if (splittage.length == 1) {
			return content == pattern;
		} else if (splittage.length == 2) {
			return content.has_prefix(splittage[0]) && content.has_suffix(splittage[1]);
		} else {
			return false;
		}
	}
	
}
