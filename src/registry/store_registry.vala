public class Slate.Registry.StoreRegistry : Object {
	private List<Slate.Registry.StoreRegistryEntry> stores = new List<Slate.Registry.StoreRegistryEntry>();
	
	public StoreRegistry.default_configuration(){
		this.add_resource_store("test://",new Slate.Store.Test());
	}
	
	public void add_resource_store(string prefix,Slate.Interface.ResourceStore store){
		stores.append(new Slate.Registry.StoreRegistryEntry(prefix,store));
	}
	
	public Slate.Interface.ResourceStore? get_closest_match(string uri){
		Slate.Interface.ResourceStore best_match = null;
		uint closest_match_length = 0;
		foreach(Slate.Registry.StoreRegistryEntry entry in stores){
			if (uri.has_prefix(entry.prefix) && entry.prefix.length > closest_match_length){
				best_match = entry.store;
				closest_match_length = entry.prefix.length;
			}
		}
		return best_match;
	}
	
}

private class Slate.Registry.StoreRegistryEntry {
	public string prefix;
	public Slate.Interface.ResourceStore store;
	
	public StoreRegistryEntry(string prefix,Slate.Interface.ResourceStore store){
		this.prefix = prefix;
		this.store = store;
	}
}
