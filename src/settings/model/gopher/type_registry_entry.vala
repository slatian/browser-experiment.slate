public class Slate.Settings.Model.Gopher.TypeRegistryEntry {
	public unichar gophertype { get; protected set; }
	public string? mimetype { get; protected set; }
	public string uri_template { get; protected set; }
	public bool mimeyte_is_suggestion { get; protected set; default = false; }
	public Slate.Document.SlateDocument.Interface.Generator.Configuration.GophertypeClass hint { get; protected set; }
	
	public TypeRegistryEntry(unichar gophertype, string? mimetype = null, string? uri_template = null, Slate.Document.SlateDocument.Interface.Generator.Configuration.GophertypeClass hint = LINK){
		this.gophertype = gophertype;	
		this.hint = hint;
		if (mimetype != null) {
			this.mimeyte_is_suggestion = mimetype.has_suffix("*") || mimeyte_is_suggestion;
			if (mimetype.has_prefix("~")){
				this.mimetype = mimetype.substring(1);
				this.mimeyte_is_suggestion = true;
			}	else {
				this.mimetype = mimetype;
			}
		} else {
			this.mimetype = null;
			this.mimeyte_is_suggestion = true;
		}
		if (uri_template != null){
			this.uri_template = uri_template;
		} else {
			this.uri_template = "gopher://{host}:{port}/{type}{selector}";
		}
	}
	
	public Slate.Settings.Model.Gopher.TypeRegistryEntry make_mimetype_suggestion(){
		this.mimeyte_is_suggestion = true;
		return this;
	}
	
	public string get_uri(string host, string port, string selector){
		if (selector.has_prefix("URL:")) {
			return selector.substring(4);
		}
		var uri = uri_template;
		uri = uri.replace("{host}",Uri.escape_string(host));
		uri = uri.replace("{port}",Uri.escape_string(port));
		uri = uri.replace("{type}",@"$gophertype");
		uri = uri.replace("{selector}",Uri.escape_string(selector,"/"));
		return uri;
	}
	
}
