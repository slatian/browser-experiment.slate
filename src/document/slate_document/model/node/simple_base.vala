public abstract class Slate.Document.SlateDocument.Model.Node.SimpleBase : Slate.Document.SlateDocument.Interface.Node, Object {
	
	protected string node_id = GLib.Uuid.string_random();
	protected string? uri = null;
	protected string? text = null;
	protected string? description = null;
	protected string? category = null;
	protected string? index_id = null;
	protected Slate.Document.SlateDocument.Model.WrapMode wrap_mode = UNKNOWN;
	
	public void set_index_id(string? index_id){
		this.index_id = index_id;
		this.node_modified(this);
	}
	
	public void set_category(string? category){
		this.category = category;
		this.node_modified(this);
	}
	
	public void set_description(string? description){
		this.description = description;
		this.node_modified(this);
	}
	
	public void set_text(string? text){
		this.text = text;
		this.node_modified(this);
	}
	
	public void set_uri(string? uri){
		this.uri = uri;
		this.node_modified(this);
	}
	
	  /////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Node //
	/////////////////////////////////////////////////
	
	public abstract Slate.Document.SlateDocument.Model.NodeType get_node_type();
	
	public virtual string get_node_id(){
		return node_id;
	}
	
	public virtual string? get_node_index_id(){
		return index_id;
	}
	
	public virtual string? get_node_category(){
		return category;
	}

	public virtual string? get_node_text(){
		return text;
	}
	
	public virtual string? get_node_description(){
		return description;
	}
	
	public virtual string? get_node_uri(){
		return uri;
	}
	
	public virtual Slate.Document.SlateDocument.Model.WrapMode get_preferred_text_wrap_mode(){
		return wrap_mode;
	}
}
