public abstract class Slate.Document.SlateDocument.Parser.Base : Slate.Document.SlateDocument.Interface.Parser, Object {
	
	protected Slate.Interface.Page.Service.Resource.Resource resource; // has to be set in constructor
	private bool _has_started = false;
	private bool _has_finished = false;
	private Slate.Document.SlateDocument.Interface.Node? root_node = null;
	
	public abstract async void parse(InputStream input_stream, string? source_uri, Cancellable? cancellable) throws Error;
	
	protected void return_root_node(Slate.Document.SlateDocument.Interface.Node? node) {
		this.root_node = node;
		this.on_root_node_updated(node);
	}
	
	  ///////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Parser //
	///////////////////////////////////////////////////
	
	public async void process(Cancellable? cancellable) throws Error {
		this._has_finished = false;
		this._has_started = true;
		var input_stream = resource.get_resource_content_stream();
		// TODO improve error handling
		if (input_stream != null) {
			try {
				yield parse(input_stream, resource.get_metadata("uri"), cancellable);
			} catch (Error e) {
				critical(e.message);
				this._has_finished = true;
				this.on_finished(root_node);
				throw e;
			}
		}
		this._has_finished = true;
		this.on_finished(root_node);
	}
	
	public Slate.Document.SlateDocument.Interface.Node? get_root_node(){
		return root_node;
	}
	
	public bool has_started(){
		return _has_started;
	}
	
	public bool has_finished(){
		return _has_finished;
	}
}
