public class Slate.Document.SlateDocument.Generator.GeminiMarkup : Slate.Document.SlateDocument.Interface.Generator.Gemini, Object {
	
	/*
		This is a document generator, wich specializes on generating slate document models from gemtext tokens.
		The document itself still has to be parsed by external code, but ths code doesn't have to know the details
		of how to generate slate document models.
	*/
	
	/*
		Initialize a markup document generator, two accumulators fo multiline text and a last_token_type variable for storing what kind of token we got in the previous call.
	*/
	private Slate.Document.SlateDocument.Generator.Markup markup;
	private string text_accumulator = "";
	private string description_accumulator = "";
	private Slate.Document.SlateDocument.Interface.Generator.GeminiToken last_token_type = TEXT;
	
	// constructor that initalizes the markdown generator
	public GeminiMarkup(string? title, string? uri){
		markup = new Slate.Document.SlateDocument.Generator.Markup(title, null, uri);
	}
	
	  /////////////////////////////////////////////////////////////
	 // Slate.Document.SlateDocument.Interface.Generator.Gemini //
	/////////////////////////////////////////////////////////////
	
	public void append_gemini_token(Slate.Document.SlateDocument.Interface.Generator.GeminiToken token_type, string content, string? description = null){
		// if we have a different token type or a preformatted block starts or ends
		if ((last_token_type != token_type)
		 || (last_token_type == PREFORMATTED_TEXT_START)
		 || (last_token_type == PREFORMATTED_TEXT_END)) {
			//handle accumulator end
			switch(last_token_type){
				// if we had something to to with preformatted text
				case PREFORMATTED_TEXT:
				case PREFORMATTED_TEXT_START:
				case PREFORMATTED_TEXT_END:
					// if a preformatted block has ended
					if ((last_token_type == PREFORMATTED_TEXT_START && token_type != PREFORMATTED_TEXT)
					 || (last_token_type == PREFORMATTED_TEXT && token_type != PREFORMATTED_TEXT_END)
					 || (last_token_type == PREFORMATTED_TEXT_END)) {
					 	// get the description and replace it with null when empty
					 	string? _description = description_accumulator;
					 	if (_description == "") {
					 		_description = null;
					 	}
					 	// create a new node for the preformatted block we just parsed
						var node = new Slate.Document.SlateDocument.Model.Node.SimpleParagraph(text_accumulator, PREFORMATTED, _description);
						// append node to the current section
						markup.append_node(node);
						// clear the accumulators so that they can be reused
						text_accumulator = "";
						description_accumulator = "";
					}
					break; //jump to the end of this switch statement
				// in case we had something to do with quotes
				case QUOTE:
					// make a quote node,
					var node = new Slate.Document.SlateDocument.Model.Node.SimpleParagraph(text_accumulator);
					node.set_category("quote");
					// append it to the current section
					markup.append_node(node);
					// and empty the accumulator
					text_accumulator = "";
					break;
				case LIST_ITEM:
					// if a link directly follows a list item it is considered part of the list
					// if the list ended
					// diabled because it causes some funny bugs all over the place
					//if (token_type != LINK) {
						// exit the list
						markup.set_list_level(0);
					//}
					break;
				default:
					break;
			}
		}
		// do different things based on the kind of data the parser got next
		switch (token_type) {
			case TEXT:
				// if we just have a line of plain text, append it as a paragraph
				markup.append_node(new Slate.Document.SlateDocument.Model.Node.SimpleParagraph(content));
				break;
			case EMPTY_LINE:
				// if we got an empty line, append an empty paragraph
				markup.append_blank_paragraph();
				break;
			case H1:
				// if we got a level 1 headline, start a new section based on level 0, with content as the headline
				markup.start_section(0, content);
				break;
			case H2:
				// if we got a level 2 headline, start a new section based on level 1, with content as the headline
				markup.start_section(1, content);
				break;
			case H3:
				// if we got a level 3 headline, start a new section based on level 2, with content as the headline
				markup.start_section(2, content);
				break;
			case LIST_ITEM:
				// if we got a list item make sure we have a level 1 list
				markup.set_list_level(1);
				// and append the content as if it was a simple paragraph
				markup.append_node(new Slate.Document.SlateDocument.Model.Node.SimpleParagraph(content));
				break;
			case LINK:
				// if we got a link token the contect is the uri and the descriptrion contains a label if the document specified one
				markup.append_node(new Slate.Document.SlateDocument.Model.Node.SimpleItem(content, description));
				break;
			case QUOTE:
			case PREFORMATTED_TEXT:
				// in case of a quote or preformatted text accumulate text to add it as one paragraph when the quote or preformatted block ends
				// if the accumulator is not empty append a linebreak, since a new token for a gemini document also means new line
				if (text_accumulator != ""){
					text_accumulator += "\n";
				}
				// append content to the accumulator
				text_accumulator += content;
				break;
			case PREFORMATTED_TEXT_START:
				// in case we are starting a preformatted block the content contains the first part of a description
				description_accumulator = content;
				break;
			case PREFORMATTED_TEXT_END:
				// and the end of a preformatted block contains the second part of a description, we append it to the description accumulator after a linebreak
				description_accumulator += "\n"+content;
				break;
			default:
				break;
		}
		// remember what came in last
		last_token_type = token_type;
	}
	
	// function to retrieve the root node
	public Slate.Document.SlateDocument.Interface.Node get_root_node(){
		return markup.root_node;
	}
	
}
