public class Slate.Document.SlateDocument.Generator.Markup : Object {
		
	/*
		This is a generic document generator wich aims to make it easier to implement markup parsers,
		especially the ones where headline and list levels can be set by the number of prefix caracters.
		
		Support for multiple levels of quoting, similar to how list levels wor is planned.
		Make sure to update the gemini markup generator when that happens.
	*/
	
	// define a private (nothing outside this class has access) stack for active section nodes
	private Slate.Util.Stack<Slate.Document.SlateDocument.Model.Node.SectionBase> sections = new Slate.Util.Stack<Slate.Document.SlateDocument.Model.Node.SectionBase>();
	
	// root node, free for use of public
	public Slate.Document.SlateDocument.Model.Node.SectionBase root_node;
	// the current section, for easy access
	private Slate.Document.SlateDocument.Model.Node.SectionBase current_section;
	// at wich level are we
	public uint current_section_level { get; private set; default=0; }
	// how many of those levels are list levels
	public uint current_list_level { get; private set; default=0; }
	
	//all levels below the limit will be considered root title candidates
	public uint autotitle_level_limit = 0;
	
	// class constructor
	public Markup(string? title, string? category, string? uri, string? description = null){
		// initalize the current_section wit some data given to the constructor function
		current_section = new Slate.Document.SlateDocument.Model.Node.SectionBase(title, category, description, uri);
		// call the set_category() function on the current_section to turn it into a document
		current_section.set_category("document");
		// the current section is also the root node, currently
		root_node = current_section;
		// if no title is set, …
		if (title == null) {
			// set the autotitle level to a high default, so that the title can be chooses from the lowest level node with a title
			autotitle_level_limit = 100;
		}
	}
	
	/*
		This function starts a new section.
		The level argument tells the function wich level the new section should be on. (Use 0 to make it a direct child of the root node).
		the interpret_category enables category specific behaviour:
			- list : automatically start a list section if the category is "list"
			         the level also becomes a list level, use 0 to start a new list, use 1 to start a sublevel, etc.
	*/
	public void start_section(uint level, string? title, string? category = null, bool interpret_category = true, string? uri = null, string? description = null, string? index_id = null){
		//autotitle
		// if there is a title and the node level is below the autotitle limit
		if (title != null && level < autotitle_level_limit){
			// set the limit to the surrent level, so that only nodes higher up in the hirarchy will be cosidered title candidates
			autotitle_level_limit = level;
			// until we find something better set the root nodes text (title fild in case of a section node) to the title of this section
			root_node.set_text(title);
		}
		// do we have a list section, and do we want special treatment
		bool is_list_section = false;
		if(interpret_category) {
			is_list_section = category == "list";
		}
		//debug(@"Starting section at $level, list_section:$is_list_section, current_section_level:$current_section_level, current_list_level:$current_list_level");
		// exit all list levels, if we care about special sections, this is not a list section and we are in a list
		if (interpret_category && (!is_list_section) && current_list_level > 0){
			set_list_level(0);
		}
		// make sure we are at the level we want to be at
		if (is_list_section) {
			// treat level as list level for the special case of a list section
			set_list_level(level);
		} else {
			// treat level as section level in any other case
			set_section_level(level);
		}
		// we are going one level up
		current_section_level++;
		if (is_list_section) {
			// also go up one list level, if this is a list section
			current_list_level++;
		}
		// create a new section object
		var new_section = new Slate.Document.SlateDocument.Model.Node.SectionBase(title, category, description, uri);
		new_section.set_index_id(index_id);
		// append new section as the next part of the current section
		current_section.append_node(new_section);
		// push the current section onto the sections stack and 
		sections.push(current_section);
		// use the new section as the current section for all operations
		current_section = new_section;
	}
	
	// function to set the section level
	public void set_section_level(uint level, string? new_section_category = null){
		print(@"Setting section level from $current_section_level to $level\n");
		// start new sections if we have to increae the current section level
		while (level > current_section_level){
			//start dummy sections at current level until we have raised to the requested level
			//  a call to this funciton can be the result of a start_section() call,
			// thats the reason we unset the interpret_category flag here, because we don't want to end up in recursion hell!
			start_section(current_section_level, null, new_section_category, false);
		}
		// pop old sections off the stack if we have to decrease the current section level
		while (level < current_section_level) {
			//end previous sections
			// since level is a uint we can't go below 0
			// go down one level
			current_section_level--;
			// decrease the list level if we are still in a list
			if (current_list_level > 0){
				current_list_level--;
			}
			// use the section higher up in the hirarchy as the current section
			// (no adding required, the old current_section was already appended to the  one on the stack)
			// we should also have current_section_level items in the stack at this point
			current_section = sections.pop();
		}
	}
	
	// append a node to the current section
	public void append_node(Slate.Document.SlateDocument.Interface.Node node){
		current_section.append_node(node);
	}
	
	// convenience funtion to append empty paragraphs in a quick way
	public void append_blank_paragraph(){
		// create paragraph node
		var blank_paragraph = new Slate.Document.SlateDocument.Model.Node.SimpleParagraph("", UNKNOWN);
		// set a category of empty
		blank_paragraph.set_category("empty");
		// append the node to the current section
		append_node(blank_paragraph);
	}
	
	// function to set the list level
	public void set_list_level(uint list_level){ //list level 0 means no list
		print(@"Setting list level from $current_list_level to $list_level\n");
		if (current_list_level != list_level) {
			// set the section level to the current_section_level minus the current list level plus the list level we want
			// list_level is guaranteed to be not greater than the current_section_level
			set_section_level((current_section_level-current_list_level)+list_level, "list");
			// update the current list level, because set_section_level doesn't always do that corrctly for us
			current_list_level = list_level;
		}
	}
}
