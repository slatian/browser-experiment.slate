public enum Slate.Document.SlateDocument.Interface.Generator.GeminiToken {
	TEXT,
	H1,
	H2,
	H3,
	QUOTE,
	PREFORMATTED_TEXT_START,
	PREFORMATTED_TEXT_END,
	PREFORMATTED_TEXT,
	LINK,
	LIST_ITEM,
	EMPTY_LINE,
	END_OF_DOCUMENT;
}
