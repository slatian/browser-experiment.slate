public interface Slate.Document.SlateDocument.Interface.Generator.Gemini : Object {
	
	public abstract void append_gemini_token(Slate.Document.SlateDocument.Interface.Generator.GeminiToken token_type, string content, string? description = null);
	public abstract Slate.Document.SlateDocument.Interface.Node get_root_node();
	
}
