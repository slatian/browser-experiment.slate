public interface Slate.Document.SlateDocument.Interface.Generator.Configuration.Gophertypes : Object {
	
	/*
		This interface specifies how the configuration for a gopher parser has to look like,
		its implementation will replace the gophertype registry from dragonstone.
	*/
	
	public abstract Slate.Document.SlateDocument.Interface.Generator.Configuration.GophertypeClass? get_gophertype_class(unichar gophertype);
	public abstract string? get_uri(unichar gophertype, string host, string port, string selector);
	
}
