public interface Slate.Document.SlateDocument.Interface.Generator.Gopher : Object {
	
	public abstract void append_gopher_token(unichar gophertype, string text, string path, string host, string port);
	public abstract Slate.Document.SlateDocument.Interface.Node get_root_node();
	
}
