public interface Slate.Document.SlateDocument.Interface.ParserFactory : Object {
	
	/*
		Implementations of this interface are supposed to give you a parser that is configured to pare the one resource you gave to the factory into a document model.
		If a name is specified it should pick the desired parser, if no name is specified the parser should be picked based on resource metadata.
	*/
	
	public abstract Slate.Document.SlateDocument.Interface.Parser? get_parser(Slate.Interface.Page.Service.Resource.Resource resource, string? parser_name = null);	
	
	
	/*
		Iterate over all parsers, optionally it can filter for parsers, that can parse a particular resource
	*/
	public abstract void foreach_parser_name(Slate.Interface.Page.Service.Resource.Resource? resource, Func<string> cb);
}
