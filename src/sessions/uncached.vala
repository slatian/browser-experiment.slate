public class Slate.Session.Uncached : Slate.Interface.Session, Object {
	private Slate.Interface.ResourceStore backend;
	private string _name = "Uncached";
	
	public Uncached(Slate.Interface.ResourceStore backend){
		this.backend = backend;
	}
	
	public Slate.Request make_download_request(string uri, bool reload=false){
		print(@"[session.uncached] making request to $uri\n");
		var request = new Slate.Request(uri,reload);
		backend.request(request);
		return request;
	}
	
	public Slate.Request make_upload_request(string uri, Slate.Resource resource, out string upload_urn = null){
		upload_urn = "urn:upload:"+GLib.Uuid.string_random();
		var request = new Slate.Request(uri).upload(resource,upload_urn);
		backend.request(request,null,true);
		return request;
	}
	
	public bool set_default_backend(Slate.Interface.ResourceStore store){
		backend = store;
		return true;
	}
	
	public Slate.Interface.ResourceStore? get_default_backend(){
		return backend;
	}
	
	public void set_name(string name){ _name = name; }
	public string get_name(){ return _name; }
	
}
