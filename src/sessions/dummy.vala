public class Slate.Session.Dummy : Slate.Interface.Session, Object {
	private string _name = "Dummy";
	
	public Slate.Request make_download_request(string uri, bool reload=false){
		var request = new Slate.Request(uri,reload);
		request.setStatus("error/dummySession");
		return request;
	}
	
	public Slate.Request make_upload_request(string uri, Slate.Resource resource, out string upload_urn = null){
		upload_urn = "urn:upload:"+GLib.Uuid.string_random();
		var request = new Slate.Request(uri).upload(resource,upload_urn);;
		request.setStatus("error/dummySession");
		return request;
	}
	
	public bool set_default_backend(Slate.Interface.ResourceStore store){
		return false;
	}
	
	public Slate.Interface.ResourceStore? get_default_backend(){
		return null;
	}
	
	public Slate.Interface.Cache? get_cache() {
		return null;
	}
	
	public void erase_cache() {}
	
	public void set_name(string name){ _name = name; }
	public string get_name(){ return _name; }
	
}
