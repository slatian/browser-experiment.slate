public class Slate.GtkUi.Widget.Viewlet.LoadingIcon : Gtk.Bin {
	
	public string default_icon = "content-loading-symbolic";
	public string connecting_icon = "network-idle-symbolic";
	public string shaking_hands_icon = "network-transmit-receive-symbolic";
	public string downloading_icon = "network-receive-symbolic";
	public string finished_icon = "text-x-generic-symbolic";
	
	private Slate.Interface.Ui.LoadingWidgetBackend backend;
	private Gtk.Image image;
	private string current_icon = "";
	
	public LoadingIcon(Slate.Interface.Ui.LoadingWidgetBackend backend){
		this.backend = backend;
		this.image = new Gtk.Image();
		backend.progress_updated.connect(update_icon);
		update_icon();
		add(image);
	}
	
	~LoadingIcon(){
		backend.progress_updated.disconnect(update_icon);
	}
	
	private void update_icon(){
		string icon = default_icon;
		if (backend.is_connecting()) {
			icon = connecting_icon;
		} else if (backend.has_transaction_ended()) {
			icon = finished_icon;
		} else if (backend.is_shaking_hands()) {
			icon = shaking_hands_icon;
		} else if (backend.is_downloading()) {
			icon = downloading_icon;
		}
		if (icon != current_icon){
			current_icon = icon;
			Timeout.add(0,() => {
				image.set_from_icon_name(icon, BUTTON);
				return false;
			});
		}
	}
	
	
}
