public class Slate.GtkUi.Widget.Viewlet.MainAddressEntry : Gtk.Box {
	
	private Slate.Interface.Ui.AddressbarBackend backend;
	
	public string reload_tooltip_label = "Please set reload_tooltip_label";
	public string go_tooltip_label = "Please set go_tooltip_label";
	
	// Widgets
	private Gtk.Button load_button;
	private Gtk.Entry address_entry;
	private Gtk.Image reload_icon = new Gtk.Image.from_icon_name("view-refresh-symbolic",BUTTON);
	private Gtk.Image go_icon = new Gtk.Image.from_icon_name("go-jump-symbolic",BUTTON);
	private string current_load_button_icon_name = "error";
	
	public MainAddressEntry(Slate.Interface.Ui.AddressbarBackend backend){
		this.backend = backend;
		
		this.homogeneous = false;
		this.spacing = 6;
		
		address_entry = new Gtk.Entry();
		address_entry.input_purpose = Gtk.InputPurpose.URL;
		address_entry.halign = Gtk.Align.FILL;
		address_entry.hexpand = true;
		address_entry.notify["text"].connect(update_load_button_icon);
		address_entry.activate.connect(on_submit);
		address_entry.focus_out_event.connect(() => {
			on_focus_out();
			return false;
		});
		pack_start(address_entry, true, true);
		
		load_button = new Gtk.Button.from_icon_name("dialog-error-symbolic");
		load_button.valign = Gtk.Align.END;
		load_button.get_style_context().add_class("suggested-action");
		load_button.clicked.connect(on_submit);
		pack_start(load_button, false, false);
		
		this.backend.address_updated.connect(address_updated);
		address_updated(this.backend.get_current_address());
	}
	
	private void update_load_button_icon(){
		string icon_name = backend.get_action_icon_for_address(address_entry.text);
		if (icon_name != current_load_button_icon_name) {
			current_load_button_icon_name = icon_name;
			switch(icon_name){
				case "reload":
					load_button.image = reload_icon;
					load_button.tooltip_text = reload_tooltip_label;
					break;
				case "go":
				default:
					load_button.image = go_icon;
					load_button.tooltip_text = go_tooltip_label;
					break;
			}
		}
	}
	
	private void on_focus_out(){
		if (address_entry.text == ""){
			var address = backend.get_current_address();
			address_updated(address);
		}
	}
	
	private void address_updated(string? uri){
		if (uri != null) {
			address_entry.text = uri;
		} else {
			address_entry.text = "";
		}
		update_load_button_icon();
	}
	
	private void on_submit(){
		string address = backend.try_address_correction(address_entry.text);
		address_entry.text = address;
		backend.submit_address(address_entry.text);
	}
	
	 ////////////////
	// public api
	
	public void reload(){
		var address = backend.get_current_address();
		if (address != null){
			address_updated(address);
		} else {
			address_updated("");
		}
	}
	
}
