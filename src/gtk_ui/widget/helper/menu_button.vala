public class Slate.GtkUi.Widget.Helper.MenuButton : Gtk.Button {

	construct {
		halign = Gtk.Align.FILL;
		get_style_context().add_class("flat");
	}
	
	public MenuButton(string labeltext){
		set_css_name("modelbutton");
		var label = new Gtk.Label(labeltext);
		label.single_line_mode = true;
		label.set_justify(Gtk.Justification.LEFT);
		label.halign = Gtk.Align.START;
		add(label);
	}
	
}
