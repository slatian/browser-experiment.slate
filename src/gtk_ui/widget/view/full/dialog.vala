public class Slate.GtkUi.Widget.View.Full.Dialog : Gtk.Box, Slate.GtkUi.Interface.View {
	
	protected Gtk.Box outer_box = new Gtk.Box(Gtk.Orientation.VERTICAL,1);
	protected Gtk.Box center_box = new Gtk.Box(Gtk.Orientation.VERTICAL,8);
	protected Gtk.ScrolledWindow scrolled_window = new Gtk.ScrolledWindow(null,null);
	private Slate.Interface.Page.Service.Dialog? dialog_ui_service = null;
	private Slate.GtkUi.Widget.Viewlet.BigDialog? dialog_widget = null;
	private string? dialog_id = null;
	
	construct{
		center_box.margin = 16;
		this.orientation = Gtk.Orientation.VERTICAL;
		outer_box.set_center_widget(center_box);
		scrolled_window.add(outer_box);
		pack_start(scrolled_window, true, true);
		this.show();
		this.scrolled_window.show_all();
	}
	
	private void remove_dialog(){
		lock (dialog_widget) {
			if (dialog_widget != null) {
				center_box.remove(dialog_widget);
			}
		}
	}
	
	private void on_dialog_removed(string dialog_id){
		lock(this.dialog_id){
			if (this.dialog_id == dialog_id) {
				remove_dialog();
			}
		}
	}
	
	  ////////////////////////////////
	 // Slate.GtkUi.Interface.View //
	////////////////////////////////
	
	public bool display_page(Slate.Interface.Page.Page? page, Slate.GtkUi.Interface.ViewContext view_context, string module_name, string? target_id){
		lock(this.dialog_id){
			if (dialog_ui_service != null){
				dialog_ui_service.dialog_removed.disconnect(on_dialog_removed);
			}
			if (page != null && target_id != null) {
				remove_dialog();
				dialog_ui_service = page.get_dialog_ui_service();
				if (dialog_ui_service != null) {
					var dialog = dialog_ui_service.get_dialog(target_id);
					if (dialog != null) {
						dialog_ui_service.dialog_removed.connect(on_dialog_removed);
						this.dialog_id = target_id;
						lock (dialog_widget) {
							dialog_widget = new Slate.GtkUi.Widget.Viewlet.BigDialog(dialog);
							dialog_widget.update_context(view_context);
							dialog_widget.show_all();
							center_box.set_center_widget(dialog_widget);
						}
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public void reload(){
		// Do nothing, dialogs can't be modified
	}
	
	public string? get_target_type(){
		return "dialog";
	}
	
	public string? get_target_id(){
		return dialog_id;
	}
	
	public string? get_title(){
		lock (dialog_widget) {
			if (dialog_widget != null) {
				return dialog_widget.get_title();
			}
		}
		return null;
	}
	
}
