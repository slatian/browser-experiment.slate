public class Slate.GtkUi.Widget.PageFrame : Gtk.Box, Slate.GtkUi.Interface.PageFrame {
	
	private Slate.Interface.Page.Tab tab;
	private Slate.GtkUi.Interface.ViewContext view_context;
	private Slate.Interface.Page.Page page;
	
	//Page Services
	private Slate.Interface.Page.Service.Resource.Service? resource_service = null;
	private Slate.Interface.Page.Service.Document.Store? document_store_service = null;
	private Slate.Interface.Page.Service.Transaction.Log? transaction_log = null;
	private Slate.Interface.Page.Service.Dialog? dialog_ui_service = null;
	
	//Temporary Widgets
	private Gtk.TextView textview = new Gtk.TextView();
	private Gtk.ScrolledWindow scrolled_debug = new Gtk.ScrolledWindow(null, null);
	private Gtk.Label status_label = new Gtk.Label("status_label");
	private Slate.GtkUi.Widget.View.Bar.SimpleLoading loading_widget = new Slate.GtkUi.Widget.View.Bar.SimpleLoading();
	
	private Gtk.Box content_stack_box = new Gtk.Box(HORIZONTAL, 0);
	private Gtk.Stack content_stack = new Gtk.Stack();
	private Gtk.StackSidebar content_stack_sidebar = new Gtk.StackSidebar();
	private List<Gtk.Widget> page_specific_views = new List<Gtk.Widget>();
	
	public PageFrame(Slate.Interface.Page.Tab tab, Slate.GtkUi.Interface.ViewContext view_context){
		this.tab = tab;
		this.view_context = view_context;
		tab.current_page_changed.connect(on_page_changed);
		on_page_changed();
		scrolled_debug.child = textview;
		scrolled_debug.vexpand = true;
		content_stack_box.pack_start(content_stack_sidebar, false, false);
		content_stack_box.pack_start(content_stack, true, true);
		content_stack_sidebar.stack = content_stack;
		content_stack.add_titled(scrolled_debug, "debug-out", "Debugging output");
		this.orientation = VERTICAL;
		this.homogeneous = false;
		this.pack_start(status_label, false, false);
		this.pack_start(loading_widget, false, false);
		this.pack_start(content_stack_box, true, true);
		textview.wrap_mode = WORD_CHAR;
		textview.monospace = true;
		this.show_all();
		loading_widget.revealed = false;
	}
	
	private void remove_widget_by_id(string widget_id){
		var widget = content_stack.get_child_by_name(widget_id);
		if (widget != null){
			content_stack.remove(widget);
			page_specific_views.remove(widget);
		}
	}
	
	private void add_widget(Gtk.Widget widget, string widget_id, string title){
		widget.show_all();
		content_stack.add_titled(widget, widget_id, title);
		page_specific_views.append(widget);
	}
	
	private void on_dialog_added(string dialog_id, Slate.Interface.Ui.Dialog dialog){
		var dialog_widget = new Slate.GtkUi.Widget.View.Full.Dialog();
		dialog_widget.display_page(this.page, this.view_context, "", dialog_id);
		string? title = dialog_widget.get_title();
		if (title == null) {
			title = "Dialog "+dialog_id;
		}
		add_widget(dialog_widget, "dialog-"+dialog_id, title);
	}
	
	private void on_dialog_removed(string dialog_id){
		remove_widget_by_id("dialog-"+dialog_id);
	}
	
	private void load_debug_tools(Slate.Interface.Page.Page page){
		Timeout.add(0,() => {
			add_widget(new Slate.GtkUi.Widget.Viewlet.Tasklist(this.page.get_tasks_service()), "taskmanager", "Page Tasks");
			return false;
		});
	}
	
	private void on_page_changed(){
		if (resource_service != null) {
			resource_service.resource_updated.disconnect(on_resource_updated);
		}
		if (document_store_service != null) {
			document_store_service.document_modified.disconnect(on_document_modified);
		}
		if (transaction_log != null) {
			transaction_log.transaction_started.disconnect(on_transaction_started);
			transaction_log.log_entry_submitted.disconnect(on_log_entry_submitted);
			transaction_log.resource_updated.disconnect(on_transaction_resource_updated);
		}
		if (dialog_ui_service != null) {
			dialog_ui_service.dialog_added.disconnect(on_dialog_added);
			dialog_ui_service.dialog_removed.disconnect(on_dialog_removed);
		}
		cleanup_views();
		//Make sure to not use the page variable above as it is unintalized the firt time this function is called
		this.page = tab.get_current_page();
		lock (resource_service) {
			this.resource_service = this.page.get_resource_service();
		}
		lock (document_store_service) {
			this.document_store_service = this.page.get_document_store_service();
		}
		lock (transaction_log) {
			this.transaction_log = this.page.get_transaction_log_service();
		}
		lock (dialog_ui_service) {
			this.dialog_ui_service = this.page.get_dialog_ui_service();
		}
		if (resource_service != null) {
			resource_service.resource_updated.connect(on_resource_updated);
			resource_service.foreach_resource(on_resource_updated);
		}
		if (document_store_service != null) {
			document_store_service.document_modified.connect(on_document_modified);
			document_store_service.foreach_document(on_document_modified);
		}
		if (transaction_log != null) {
			transaction_log.transaction_started.connect(on_transaction_started);
			transaction_log.log_entry_submitted.connect(on_log_entry_submitted);
			transaction_log.resource_updated.connect(on_transaction_resource_updated);
		}
		if (dialog_ui_service != null) {
			dialog_ui_service.dialog_added.connect(on_dialog_added);
			dialog_ui_service.dialog_removed.connect(on_dialog_removed);
		}
		load_debug_tools(this.page);
		loading_widget.revealed = false;
		loading_widget.display_page(page, this.view_context, "loading_widget", null);
	}
	
	// clean views up, so that they are ready to be reloaded with new content
	private void cleanup_views(){
		Timeout.add(0,() => {
			textview.buffer.text = "";
			foreach(var widget in page_specific_views){
				content_stack.remove(widget);
			}
			page_specific_views = new List<Gtk.Widget>();
			return false;
		});
	}
	
	private void on_transaction_started(string transaction_id, string uri, string verb){
		append_text(@"Transaction $transaction_id started: $verb $uri\n");
	}
	
	private void on_log_entry_submitted(string transaction_id, Slate.Interface.Page.Service.Transaction.LogEntry entry){
		string text = @"[$transaction_id][$(entry.protocol)][$(entry.handler)] $(entry.topic) $(entry.key)";
		if (entry.statuscode != null) {
			text += @" ($(entry.statuscode))";
		}
		if (entry.uri != null) {
			text += @" $(entry.uri)";
		}
		if (entry.data != null) {
			text += @" $(entry.data)";
		}
		if (entry.message != null) {
			text += @" '$(entry.message)'";
		}
		append_text(text);
		if (entry.topic == "state") {
			Timeout.add(0,() => {
				loading_widget.revealed = (entry.key == "connecting" || entry.key == "shaking_hands" || entry.key == "loading" || entry.key == "downloading");
				return false;
			});
		}
		if (entry.topic == "redirect") {
			append_text(@"Redirect to: $(entry.uri)");
		}
	}
	
	private void on_transaction_resource_updated(string resource_id, Slate.Interface.Page.Service.Transaction.Resource resource){
		// Currently unused
	}
	
	private void set_status_label(string text){
		Timeout.add(0,() => {
			status_label.label = text;
			return false;
		});
	}
	
	private void append_text(string text){
		Timeout.add(0,() => {
			textview.buffer.text += text+"\n";
			return false;
		});
	}
	
	private void on_resource_updated(string resource_name, Slate.Interface.Page.Service.Resource.Resource? resource){
		Timeout.add(0,() => {
			lock(resource_service) {
				if (this.resource_service != null){
					remove_widget_by_id("resource-"+resource_name);
					if (resource != null) {
						//set_status_label(@"Resource updated $resource_name $(resource.get_metadata("mimetype"))");
						var mimetype = resource.get_metadata("mimetype");
						if (mimetype != null){
							Slate.GtkUi.Interface.ResourceView? resource_viewlet = null;
							if (mimetype.has_prefix("text/")) {
								//if (mimetype.has_prefix("text/gemini")) {
								//	resource_viewlet = new Slate.GtkUi.Widget.Viewlet.Resource.GeminiText();
								//} else if (mimetype.has_prefix("text/dragonstone-directory")) {
								//	resource_viewlet = new Slate.GtkUi.Widget.Viewlet.Resource.DragonstoneFileListing();
								//} else {
									resource_viewlet = new Slate.GtkUi.Widget.Viewlet.Resource.PlainText();
								//}
							}
							if (mimetype.has_prefix("image/")) {
								resource_viewlet = new Slate.GtkUi.Widget.Viewlet.Resource.Image();
							}
							if (resource_viewlet != null) {
								var resource_view = new Slate.GtkUi.Widget.View.Full.Resource(resource_viewlet);
								resource_view.display_page(this.page, this.view_context, "resource-"+resource_name, resource_name);
								resource_view.show_all();
								string? title = resource_view.get_title();
								if (title == null) {
									title = "Resource "+resource_name;
								}
								content_stack.add_titled(resource_view, "resource-"+resource_name, title);
								page_specific_views.append(resource_view);
							}
						}
					}
				}
			}
			return false;
		});
	}
	
	private void on_document_modified(string name, Slate.Document.SlateDocument.Interface.Node? node){
		Timeout.add(0,() => {
			lock(document_store_service){
				remove_widget_by_id("document-"+name);
				var document_view = new Slate.GtkUi.Widget.View.Full.Document();
				document_view.display_page(this.page, this.view_context, "document-"+name, name);
				document_view.show_all();
				string? title = document_view.get_title();
				if (title == null) {
					title = "Document "+name;
				} else {
					title = "[D] "+title;
				}
				content_stack.add_titled(document_view, "document-"+name, title);
				page_specific_views.append(document_view);
			}
			return false;
		});
	}
	
	  /////////////////////////////////////
	 // Slate.GtkUi.Interface.PageFrame //
	/////////////////////////////////////
	
	public void set_global_view_context(Slate.GtkUi.Interface.ViewContext view_context){
		this.view_context = view_context;
	}
	
	public Slate.Interface.Page.Tab get_tab(){
		return tab;
	}
	
}
