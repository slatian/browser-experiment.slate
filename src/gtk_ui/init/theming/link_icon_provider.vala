public class Slate.GtkUi.Init.Theming.LinkIconProvider {
	
	public static Slate.GtkUi.Interface.Theming.LinkIconProvider get_default_configuration(){
		var icon_provider = new Slate.GtkUi.Theming.RuleBasedIconProvider();
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("/**/",0,"folder"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("mailto:",0,"mail-message-new"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("http:",0,"text-html"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("https:",0,"text-html"));
		// Replace with mimeguesses
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("gopher:/0*/**",0,"text-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("gopher:/1*/**",0,"folder"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("gopher:/g*/**",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("gopher:/I*/**",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("gopher:/p*/**",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.txt",0,"text-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.zip",0,"document-save"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.png",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.gif",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.jpg",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.jpeg",0,"image-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.mp3",0,"audio-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.ogg",0,"audio-x-generic"));
		icon_provider.uri_patterns.prepend(new Slate.Util.UriPattern.from_string("**/*.wav",0,"audio-x-generic"));
		return icon_provider;
	}
	
}
