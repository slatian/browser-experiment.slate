public interface Slate.GtkUi.Interface.PageFrame : Gtk.Widget {
	
	public abstract void set_global_view_context(Slate.GtkUi.Interface.ViewContext view_context);
	
	public abstract Slate.Interface.Page.Tab get_tab();
	
}
