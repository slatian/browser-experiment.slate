public interface Slate.GtkUi.Interface.Theming.HypertextViewThemeProvider : Object {
	
	public abstract Slate.GtkUi.Interface.Theming.HypertextViewTheme? get_theme(string content_type, string uri);
	public abstract Slate.GtkUi.Interface.Theming.HypertextViewTheme get_default_theme();
	
}
