public interface Slate.GtkUi.Interface.TokenRenderer : Object {
	
	/*
		This interface is here to get a flat stream of text, along with some styling information and links onto a rendering surface.
		It is intended to be used by a modified hypertext content widget from dragonstone.
	*/
	
	public abstract void start_paragraph(string style_class, bool preformatted, string? description, uint level);
	public abstract void append_text(string text, string? link_uri);
	public abstract void end_paragraph();
	
	public abstract void reset_renderer();
}
