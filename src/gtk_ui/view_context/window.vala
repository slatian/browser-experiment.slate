public class Slate.GtkUi.ViewContext.Window : Object, Slate.GtkUi.Interface.ViewContext {
	
	private Slate.GtkUi.Widget.Window window;
	public Slate.GtkUi.Interface.ViewContext application_view_context;
	
	public Window(Slate.GtkUi.Widget.Window window, Slate.GtkUi.Interface.ViewContext application_view_context){
		this.window = window;
		this.application_view_context = application_view_context;
	}
	
	  /////////////////////////////////////////////
	 // Slate.GtkUi.Interface.ViewContext //
	/////////////////////////////////////////////
	
	public void open_uri_in_new_tab(string uri, Slate.Interface.Page.Page? from_page, string? target_container){
		window.open_uri_in_new_tab(uri, target_container);
	}
	
	public void open_uri_in_new_window(string uri, Slate.Interface.Page.Page? from_page, string? target_container){
		application_view_context.open_uri_in_new_window(uri, from_page, target_container);
	}
	
	public Slate.Interface.Localization.Translation get_translation(){
		return application_view_context.get_translation();
	}
	
	public Slate.GtkUi.Interface.Theming.LinkIconProvider? get_link_icon_provider(){
		return application_view_context.get_link_icon_provider();
	}
	
}
