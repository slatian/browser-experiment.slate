public class Slate.GtkUi.ViewContext.Application : Object, Slate.GtkUi.Interface.ViewContext {
	
	private Slate.GtkUi.Application application;
	public Slate.Interface.Localization.Translation translation;
	public Slate.GtkUi.Interface.Theming.LinkIconProvider? link_icon_provider = null;
	
	public Application(Slate.GtkUi.Application application, Slate.Interface.Localization.Translation translation){
		this.application = application;
		this.translation = translation;
	}
	
	  /////////////////////////////////////////////
	 // Slate.GtkUi.Interface.ViewContext //
	/////////////////////////////////////////////
	
	public void open_uri_in_new_tab(string uri, Slate.Interface.Page.Page? from_page, string? target_container){
		application.open_uri_in_new_tab(uri, target_container);
	}
	
	public void open_uri_in_new_window(string uri, Slate.Interface.Page.Page? from_page, string? target_container){
		application.open_uri_in_new_window(uri, target_container);
	}
	
	public Slate.Interface.Localization.Translation get_translation(){
		return translation;
	}
	
	public Slate.GtkUi.Interface.Theming.LinkIconProvider? get_link_icon_provider(){
		return link_icon_provider;
	}
}
