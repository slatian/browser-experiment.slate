public class Slate.GtkUi.Application : Gtk.Application {
	
	private Slate.GtkUi.ViewContext.Application? view_context = null;
	private Slate.Interface.Core? core = null;
	
	public Application() {
		Object (
			application_id: "com.gitlab.baschdel.slate",
			flags: ApplicationFlags.CAN_OVERRIDE_APP_ID //| ApplicationFlags.HANDLES_COMMAND_LINE
		);
	}
	
	 /////////////////////////////
	// view_context interface
	
	public void open_uri_in_new_tab(string uri, string? target_container){
		Slate.GtkUi.Widget.Window? window = (Slate.GtkUi.Widget.Window) get_active_window();
		if (window != null) {
			window.open_uri_in_new_tab(uri, target_container);
		} else {
			this.open_uri_in_new_window(uri, target_container);
		}
	}
	
	public void open_uri_in_new_window(string uri, string? target_container){
		Slate.GtkUi.Widget.Window window = build_window();
		window.open_uri_in_new_tab(uri, target_container);
	}
	
	 //////////////////////
	// internal stuff
	
	public void initalize() {
		lock (core) {
			if (core == null) {
				// If you want to use a different core, change it here
				core = new Slate.Core.Default();
			}
			if (view_context == null) {
				var translation = Slate.Init.Localization.English.get_translation();
				view_context = new Slate.GtkUi.ViewContext.Application(this, translation);
				view_context.link_icon_provider = Slate.GtkUi.Init.Theming.LinkIconProvider.get_default_configuration();
			}
		}
	}
	
	
	
	protected override void activate() {
		info("Application activated");
		initalize();
		//TODO: us settings object to get the initial new window page
		this.open_uri_in_new_window("test://",null);
	}
	
	/*
	protected override int command_line(ApplicationCommandLine command_line) {
		initalize();
		Slate.Window? window = (Slate.Window) get_active_window();
		bool new_window = false;
		if (window == null) {
			window = build_window();
			new_window = true;
		}
		string session_id = "core.default";
		bool next_is_sessionid = false;
		bool firstarg = true;
		bool uri_opened = false;
		foreach (string arg in command_line.get_arguments()){
			if (firstarg) {
				firstarg = false;
			} else if (next_is_sessionid) {
				session_id = arg;
			} else if (arg == "--new-window") {
				if (!new_window) {
					if (!uri_opened) {
						window.add_new_tab();
					}
					uri_opened = false;
					window = build_window();
				}
				new_window = false;
			} else if (arg == "--session") {
				next_is_sessionid = true;
			} else {
				string uri = arg;
				var pwd = command_line.get_cwd();
				if (pwd != null) {
					if (!pwd.has_suffix("/")) {
						pwd = pwd+"/";
					}
					uri = Slate.Util.Uri.join("file://"+pwd, arg);
				}
				window.add_tab(uri,session_id);
				uri_opened = true;
				new_window = false;
			}
		}
		if (!uri_opened) {
			window.add_new_tab();
		}
		return 0;
	}
	
	protected void on_shutdown() {
		var cache = (super_registry.retrieve("core.stores.cache") as Slate.Interface.Cache);
		if (cache != null){ cache.erase(); }
		var sessions = (super_registry.retrieve("core.sessions") as Slate.Registry.SessionRegistry);
		if (sessions != null){ sessions.erase_all_caches(); }
		var bookmark_bridge = (super_registry.retrieve("bookmarks.settings_bridge") as Slate.Settings.Bridge.Bookmarks);
		if (bookmark_bridge != null){ bookmark_bridge.export(); }
	}
	*/
	
	private Slate.GtkUi.Widget.Window build_window() {
		var window = new Slate.GtkUi.Widget.Window(this, view_context, core);
		add_window(window);
		return window;
	}
}
