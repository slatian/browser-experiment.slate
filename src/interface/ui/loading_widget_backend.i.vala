public interface Slate.Interface.Ui.LoadingWidgetBackend : Object {

	public signal void address_updated(string? new_address); //intentionally the same as in the address_bar_backend
	public signal void transaction_updated(string? new_transaction_id);
	public signal void resource_updated(string? new_resource_id);
	public signal void progress_updated();
	
	public abstract string? get_current_address(); // intentionally the same as in the address_bar_backend
	public abstract string? get_transaction_id();
	public abstract string? get_resource_id();
	
	public abstract double? get_overall_progress(); //between 0 and 1
	
	public abstract bool is_connecting();
	public abstract bool is_shaking_hands();
	public abstract bool has_transaction_ended();
	
	public abstract bool is_downloading();
	public abstract uint64 get_bytes_downloaded();
	public abstract uint64? get_expected_bytes_downloaded();
	public abstract double? get_current_download_speed();
	public abstract double? get_average_download_speed();
	
	public virtual bool trigger_action(string action){ return false; }
	public virtual string[] get_available_actions(){ return {}; }
	
}
