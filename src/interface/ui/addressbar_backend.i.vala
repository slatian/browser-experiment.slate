public interface Slate.Interface.Ui.AddressbarBackend : Object {
	
	public signal void address_updated(string? new_address); //intentionally the same as in the loading widget backend
	
	public abstract string? get_current_address(); //intentionally the same as in the loading widget backend
	public abstract string get_action_icon_for_address(string address);
	public abstract void submit_address(string address);
	
	public abstract string try_address_correction(string address);
	 
}
