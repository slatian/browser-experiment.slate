public interface Slate.Interface.Page.Service.Dialog : Object {
	
	/*
		This service just holds all dialogs together in one place so that they can be shared
		It will also be resposible for giving dialogs context
	*/
	
	public signal void dialog_added(string dialog_id, Slate.Interface.Ui.Dialog dialog);
	public signal void dialog_removed(string dialog_id);
	
	public abstract string add_dialog(Slate.Interface.Ui.Dialog dialog, string? spawning_task_id);
	public abstract bool remove_dialog(string dialog_id);
	
	public abstract void foreach_dialog(HFunc<string,Slate.Interface.Ui.Dialog> cb);
	public abstract Slate.Interface.Ui.Dialog? get_dialog(string dialog_id);
	public abstract string? get_spawning_task(string dialog_id);
	
}
