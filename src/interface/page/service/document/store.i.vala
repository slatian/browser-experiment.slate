public interface Slate.Interface.Page.Service.Document.Store : Object {
	
	/*
		Use the following format if you are a parser for document names:
		parserid<document_name
	*/
	
	public signal void document_modified(string name, Slate.Document.SlateDocument.Interface.Node? node);
	
	public abstract void store_document(string name, Slate.Document.SlateDocument.Interface.Node node);
	public abstract void remove_document(string name);
	
	public abstract void foreach_document(HFunc<string,Slate.Document.SlateDocument.Interface.Node> cb);
	
	public abstract Slate.Document.SlateDocument.Interface.Node? retrieve_document(string name);
	
}
