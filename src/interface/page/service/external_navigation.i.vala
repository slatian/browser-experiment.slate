public interface Slate.Interface.Page.Service.ExternalNavigation : Object {
	
	/*
		This interface is for navigation to other pages, the requests will be passed on to the tab 
		or an other appropriate handler
	*/
	
	/*
		This interface used to include an api for querying secondary navigation targets,
		this will be moved to a new, optional service that can choose and rank handlers
		based on the target uri.
	*/
	
	// The module_name is used for logging.
	
	public abstract void go_to_uri(string uri, string module_name);
	
	
}
