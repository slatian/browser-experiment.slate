public interface Slate.Interface.Page.Service.Log.Logged : Object {
	
	public signal void entry_submitted(string module_name, string system_name, string log_entry_type, string [] args, string message, string channel);
	
}
