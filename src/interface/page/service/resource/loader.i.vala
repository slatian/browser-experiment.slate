public interface Slate.Interface.Page.Service.Resource.Loader : Object {

	/*
		This interface provides a general api to simply load resources into a resource provider.
		The more detailed configuration of how exactly the resource is loaded is left to the loader
		implementation to make it easy to swap out the backend while not having to rewrite the core
		frontend.
	*/
	
	//returns the transaction id of the initiated request
	public abstract string? load_resource(string uri, bool reload, Slate.Interface.Page.Service.Transaction.Log transaction_log);
}
