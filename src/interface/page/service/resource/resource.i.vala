public interface Slate.Interface.Page.Service.Resource.Resource : Object {
	
	public abstract bool is_available();
	
	// Resource metadata must be static.
	// If the metadata changes submit a different resource
	// Changing metadata is best queried using a dedicated service
	public abstract string? get_metadata(string key);
	public abstract void foreach_metadata_key(Func<string> cb);
	
	// if false the resource will return false for is_available after the first get_resource_content_stream()
	// get resource_content_stream will also return null
	// meatdata will still be available
	public abstract bool is_multiuse();
	public abstract InputStream? get_resource_content_stream();
	
}
