public interface Slate.Interface.Page.Page : Object {

	// Returns a settings provider, that contains the persistant configuration for pages, overlayed with local changes
	// Writing will only make local changes that won't be saved
	public abstract Slate.Interface.Settings.Provider get_page_settings_provider();
	
	// Returns the persistant configuration for pages wich will be used for all pages in the same configuration space
	// Use this to implement settings pages otherwise don't use it
	public abstract Slate.Interface.Settings.Provider? get_persistant_page_settings_provider();
	
	// Core services
	public abstract Slate.Interface.Page.Service.Metadata get_metadata_service();
	public abstract Slate.Interface.Page.Service.Syncronisation get_syncronisation_service();
	public abstract Slate.Interface.Page.Service.InternalNavigation get_internal_navigation_service();
	public abstract Slate.Interface.Page.Service.ExternalNavigation get_external_navigation_service();
	public abstract Slate.Interface.Page.Service.Tasks.Service get_tasks_service();
	public abstract Slate.Interface.Page.Service.PagePlugin.Index get_page_plugin_index_service();
	
	// Optinal services
	public abstract Slate.Interface.Page.Service.LinearHistory? get_linear_history_service();
	public abstract Slate.Interface.Page.Service.Resource.Service? get_resource_service();
	public abstract Slate.Interface.Page.Service.Resource.Loader? get_resource_loader_service();
	public abstract Slate.Interface.Page.Service.Transaction.Log? get_transaction_log_service();
	// Document services
	public abstract Slate.Interface.Page.Service.Document.Store? get_document_store_service();
	
	//Container services
	public abstract Slate.Interface.Page.Service.Containers? get_continers_service();
	public abstract Slate.Interface.Page.Container? get_own_container();
	public abstract string? get_own_container_id();
	
	// Optional ui services
	public abstract Slate.Interface.Page.Service.Dialog? get_dialog_ui_service();

}
