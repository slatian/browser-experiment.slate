public interface Slate.Interface.Page.Container : Object {
	
	/*
		A container is the thing that construct and configure tabs.
		Tabs are the thingys that contain pages.
	*/

	// Returns the name of the container wich can then be used in the ui
	// This name should be changeable by the user
	public abstract string get_container_name();
	public abstract string get_container_landing_page_address();
	
	public abstract bool can_construct_tab_for(string address);
	
	//Lets assume a container could be in multiple container registry services
	//This approach also saves us a circular reference
	public abstract Slate.Interface.Page.Tab? construct_tab(string address, Slate.Interface.Page.Service.Containers? containers_service = null, string? own_container_id = null);
	
}
