public interface Slate.Interface.Session : Object {

	public virtual Slate.Interface.Cache? get_cache() { return null; }
	public abstract bool set_default_backend(Slate.Interface.ResourceStore store); //returns true on success
	public abstract Slate.Interface.ResourceStore? get_default_backend();
	
	public abstract Slate.Request make_download_request(string uri, bool reload=false);
	public abstract Slate.Request make_upload_request(string uri, Slate.Resource resource, out string upload_urn = null);
	
	public virtual void erase_cache() {}
	
	public abstract void set_name(string name);
	public abstract string get_name();
	
}
